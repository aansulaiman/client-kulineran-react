import { useState, useEffect } from "react"
import { useParams, useNavigate } from "react-router-dom"
import Footer from "../../components/Footer/Footer"
import Navbar from "../../components/Navbar/Navbar"

const FoodDetail = () => {

    const [foodDetail, setFood] = useState([''])
    const [jumlahPesan, setJumlahPesan] = useState('')
    const [keterangan, setKeterangan] = useState('')
    const navigate = useNavigate()

    // use effect
    useEffect(() => {
        // fetch data from API
        fetchDetail()
    }, [])

    let { id } = useParams()

    const fetchDetail = async () => {
        const response = await fetch(`http://localhost:8080/products/${id}`)
        const data = await response.json()
        setFood(data)
    }

    const addToCart = async (e) => {
        e.preventDefault();
        const nama = foodDetail.nama
        const kode = foodDetail.kode
        const harga = foodDetail.harga
        const gambar = foodDetail.gambar
        const dataToCart = { kode, nama, harga, gambar, jumlahPesan, keterangan }

        await fetch('http://localhost:8080/keranjang', {
            method: "POST",
            body: JSON.stringify(dataToCart),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        navigate('/cart')
    }

    return (
        <div>
            <Navbar />
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <img src={'../assets/images/' + foodDetail.gambar} className="img-fluid" alt={foodDetail.name} />
                    </div>
                    <div className="col-lg-6">
                        <h3>{foodDetail.nama}</h3>
                        <p className="fw-light">Rp. {foodDetail.harga}</p>

                        <form onSubmit={addToCart}>
                            <div className="mb-3">
                                <label className="form-label">Jumlah Pesan</label>
                                <input type="text" name="jumlah_pesan" value={jumlahPesan} onChange={(e) => setJumlahPesan(e.target.value)} className="form-control" />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Keterangan</label>
                                <textarea className="form-control" name="keterangan" value={keterangan} onChange={(e) => setKeterangan(e.target.value)} rows="3" placeholder="contoh: pedas, sedang, telur dadar"></textarea>
                            </div>
                            <button className="btn btn-success">Masukan Keranjang</button>
                        </form>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default FoodDetail