import Navbar from "../../components/Navbar/Navbar";
import Product from "../../components/Product/Product";
import Footer from "../../components/Footer/Footer";

const Foods = () => {
    return (
        <div>
            <Navbar />
            <div className="container">
                <div className="row mt-5">
                    <div className="col">
                        <h3>Our <strong className="text-success">Foods</strong> For You!</h3>
                        <p className="fw-light">Amazing foods Amazing foods Amazing foods!</p>
                    </div>
                </div>

                <Product />
            </div>

            <Footer />
        </div>
    )
}

export default Foods