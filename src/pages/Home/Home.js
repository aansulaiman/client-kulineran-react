// 
// import { useEffect } from "react";

// component
import Navbar from "../../components/Navbar/Navbar";
import BestProduct from "../../components/BestProduct/BestProduct";
import Footer from "../../components/Footer/Footer";

// image
import Breakfast from "../../assets/images/breakfast.svg"

const Home = () => {

    return (
        <div>
            <Navbar />
            <div className="container">
                <div className="row align-items-center mt-5">
                    <div className="col mt-4">
                        <h1>Amazing <span className="text-success fw-bold">Foods</span> for Creative People!</h1>
                        <p className="fw-light mt-2">Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food Amazing Food</p>
                        <button type="button" className="btn btn-success mt-3">Get Amazing Foods</button>
                    </div>
                    <div className="col">
                        <img src={Breakfast} alt="Logo" className="img-fluid" />
                    </div>
                </div>

                <br /><br /><br />
                <div className="row">
                    <div className="col">
                        <h3>Amazing <strong className="text-success">Foods</strong> For You!</h3>
                        <p className="fw-light">Amazing foods Amazing foods Amazing foods Amazing foods Amazing foods Amazing foods</p>
                    </div>
                </div>

                <BestProduct />

            </div>
            <Footer />
        </div>
    )
};
export default Home;