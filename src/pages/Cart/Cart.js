import { useEffect, useState } from "react"
import { Link } from "react-router-dom"

// import FetchData from "../../utils/FetchData"
import Footer from "../../components/Footer/Footer"
import Navbar from "../../components/Navbar/Navbar"

const Cart = () => {

    const [dataCart, setDatacart] = useState([''])
    const [namaPemesan, setNamaPemesan] = useState('')
    const [nomorMeja, setNomorMeja] = useState('')

    // use effect
    useEffect(() => {
        // fetch data from API
        fetchDataCart()
    }, [])

    const fetchDataCart = async () => {
        const response = await fetch('http://localhost:8080/keranjang')
        const data = await response.json()
        setDatacart(data)
    }

    const deleteItems = async (foodId) => {
        await fetch(`http://localhost:8080/keranjang/${foodId}`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            }
        })
        fetchDataCart()
    }

    const totalHarga = () => {
        let result = 0

        dataCart.forEach((item) => {
            result += item.harga * item.jumlahPesan
        })

        return result
    }

    console.log(dataCart)

    totalHarga(dataCart)

    const deleteKeranjang = async (data) => {
        data.forEach((item) => {
            fetch(`http://localhost:8080/keranjang/${item.id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        })
    }

    const pesanMakanan = async (e) => {
        e.preventDefault()

        // const totalPesanan = totalHarga(dataCart)

        const data = { namaPemesan, nomorMeja, dataCart }

        await fetch('http://localhost:8080/pesanan', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }

        })

        deleteKeranjang(dataCart)
    }

    return (
        <div>
            <Navbar />
            <div className="container">
                <div className="row mt-3">
                    <div className="col">
                        <h3>Your <strong className="text-success fw-bold">Cart!</strong></h3>
                    </div>
                </div>

                {
                    (dataCart.length) ? dataCart.map((item, index) => {
                        return <div className="row border-bottom mt-4 align-items-center" key={index}>
                            <div className="col-lg-2">
                                <img src={`../../assets/images/${item.gambar}`} alt={item.nama} className="img-fluid rounded shadow mb-2" width="150" />
                            </div>
                            <div className="col">
                                <div className="row">
                                    <div className="col-lg-3">
                                        <h4>{item.nama}</h4>
                                    </div>
                                    <div className="col">
                                        <button type="button" className="btn" onClick={() => deleteItems(item.id)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="red" d="M10 18a1 1 0 0 0 1-1v-6a1 1 0 0 0-2 0v6a1 1 0 0 0 1 1ZM20 6h-4V5a3 3 0 0 0-3-3h-2a3 3 0 0 0-3 3v1H4a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h8a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2ZM10 5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v1h-4Zm7 14a1 1 0 0 1-1 1H8a1 1 0 0 1-1-1V8h10Zm-3-1a1 1 0 0 0 1-1v-6a1 1 0 0 0-2 0v6a1 1 0 0 0 1 1Z" /></svg>
                                        </button>
                                    </div>
                                </div>
                                <p className="fw-light">Rp. <strong>{item.harga}</strong></p>
                                <p>Qty : {item.jumlahPesan}</p>
                            </div>
                        </div>
                    }) : <div className="row align-items-center">
                        <div className="col text-end">
                            <img src="../../assets/images/no_data.svg" alt="" className="img-fluid" width="200" />
                        </div>
                        <div className="col">
                            <h2>Ooopps!! your cart is empty</h2>
                            <Link to="/foods" className="btn btn-success text-decoration-none mt-4">Buy Amazing foods</Link>
                        </div>
                    </div>

                }

                <div className="row mt-5">
                    <div className="col">
                        <label className="form-label">Total harga : Rp. <strong>{totalHarga()}</strong></label>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-4">
                        <form onSubmit={pesanMakanan}>
                            <div className="mb-3">
                                <label className="form-label">Nama Pemesan</label>
                                <input type="text" name="nama" value={namaPemesan} onChange={(e) => setNamaPemesan(e.target.value)} className="form-control" required />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Nomor Meja</label>
                                <input type="number" name="noMeja" value={nomorMeja} onChange={(e) => setNomorMeja(e.target.value)} className="form-control" required />
                            </div>
                            <button className="btn btn-success">Pesan Makanan Sekarang</button>
                        </form>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Cart