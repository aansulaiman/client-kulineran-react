import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./pages/Home/Home";
import Cart from "./pages/Cart/Cart";
import Foods from "./pages/Foods/Foods";
import FoodDetail from "./pages/Foods/FoodDetail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/foods" element={<Foods />}></Route>
        <Route path="/cart" element={<Cart />}></Route>
        <Route path="/foods/:id" element={<FoodDetail />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
