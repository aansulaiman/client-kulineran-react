const FetchData = async (http) => {
    const response = fetch(http)
    const data = await JSON.stringify(response)
    return data
}

export default FetchData