const Footer = () => {
    return (
        <div className="mt-5">
            <hr />
            <div className="container text-center">
                <h6>All Right Reserved @2023</h6>
            </div>
        </div>
    )
}

export default Footer