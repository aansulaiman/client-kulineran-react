import { useEffect, useState } from "react"
import { Link } from "react-router-dom"

const Product = () => {

    // use state
    const [bestProduct, setProduct] = useState([''])

    // use effect
    useEffect(() => {
        // fetch data from API
        fetchBestProduct()
    }, [])

    const fetchBestProduct = async () => {
        const response = await fetch('http://localhost:8080/products')
        const data = await response.json()
        setProduct(data)
    }

    return (
        <div>
            <div className="row">
                {
                    bestProduct.map((product, index) => {
                        return <div className="col" key={index} >
                            <div className="card">
                                <img src={'../assets/images/' + product.gambar} className="card-img-top" alt={product.name} />
                                <div className="card-body">
                                    <h5 className="card-title">{product.nama}</h5>
                                    <p className="card-text">Rp. {product.harga}</p>
                                    <Link to={"/foods/" + product.id} className="btn btn-primary">Pesan Sekarang</Link>
                                </div>
                            </div>
                        </div>
                    })
                }

            </div>
        </div>
    )
}

export default Product